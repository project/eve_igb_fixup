<?php
require_once './includes/session.inc';

if (!(strpos($_SERVER['HTTP_USER_AGENT'],"EVE-minibrowser") === FALSE)) {
  // Force a reset of the cookie domain to let igb cope.
  ini_set('session.cookie_domain', '');
}